const { ClickListener } = require("node-nuimo-click");
const fs = require('fs-extra');
const http = require("http");

let Service, Characteristic, hapPort, hapPin;

function isLightBulb(uuid){
  return uuid == "00000043-0000-1000-8000-0026BB765291" || uuid == "43";
}
function isNameCharacteristic(uuid){
  return uuid == "00000023-0000-1000-8000-0026BB765291" || uuid == "23";
}
function isOnCharacteristic(uuid){
  return uuid == "00000025-0000-1000-8000-0026BB765291" || uuid == "25";
}
function isBrightnessCharacteristic(uuid){
  return uuid == "00000008-0000-1000-8000-0026BB765291" || uuid == "8";
}
function isHueCharacteristic(uuid){
  return uuid == "00000013-0000-1000-8000-0026BB765291" || uuid == "13";
}
function isSaturationCharacteristic(uuid){
  return uuid == "0000002F-0000-1000-8000-0026BB765291" || uuid == "2F";
}

module.exports = function(homebridge){
  Service = homebridge.hap.Service;
  Characteristic = homebridge.hap.Characteristic;

  let config = fs.readJsonSync(homebridge.user.configPath());
  hapPort = config.bridge.port;
  hapPin = config.bridge.pin;

  homebridge.registerPlatform("homebridge-nuimo-click", "NuimoClick", NuimoClick);
}

// === utils =============

async function hapCall(endPoint, putData){
  return await new Promise(resolve => {
    let url = "http://localhost:"+hapPort+"/"+endPoint;
    let sendBody = null;
    let opts = {};
    if(putData){
      sendBody = JSON.stringify(putData);
      opts.method = "PUT";
      opts.headers = {
        "Content-Length": sendBody.length,
        authorization: hapPin,
      }
    }
    let req = http.request(url, opts, res => {
      let recvBody = "";
      res.on("data", chunk => {
        recvBody += chunk;
      });
      res.on("end", _ => {
        if(res.statusCode == 200){
          resolve(JSON.parse(recvBody));
        }else{
          resolve();
        }
      });
    });
    req.on("error", e => {
      console.log("error sending to homebridge", url, putData, e);
      resolve();
    });
    if(putData) req.write(sendBody);
    req.end();
  });
}

// === NuimoClick Platform =============

class NuimoClick{
  constructor(log, config){
    this._accessories = [];

    let devices = config.devices || [];
    for(const device of devices){
      this._accessories.push(new NuimoClickAccessory(device));
    }

    this.clickListener = new ClickListener({
      serialPath: config.serialPort,
      logIds: config.logIds,
    });
    this.clickListener.onUpdateState((pressedButtons, deviceId) => {
      for(const accessory of this._accessories){
        accessory.onUpdateState(pressedButtons, deviceId);
      }
    });

    this.findLightTries = 0;
    this.findLightsInterval = setInterval(_ => {
      this.findLights();
    }, 1000*60);

    setTimeout(_ => {
      this.findLights();
    }, 1000*10)
  }

  async findLights(){
    this.findLightTries++;
    let foundAllLights = this._accessories.every(a => a.hasFoundAllLightIds);
    if(foundAllLights || this.findLightTries > 5){
      clearInterval(this.findLightsInterval);
      return;
    }

    let response = await hapCall("accessories");
    for(const accessory of this._accessories){
      accessory.accessoriesResponse(response.accessories);
    }
  }

  getServices(){
    return [];
  }

  accessories(cb){
    cb(this._accessories);
  }
}

// === NuimoClick Accessory =============

class NuimoClickAccessory{
  constructor(config){
    this.deviceId = config.id;
    this.name = config.name;
    this.services = [];
    this.buttonMap = [];

    this.controlLightIds = config.controlLightIds || [];
    this.controlLightNames = config.controlLights || [];
    this.setHasFoundAllLightIds();
    this.controlLightCharacteristics = [];

    this.createButton("circle", 1);
    this.createButton("star", 2);
    this.createButton("minus", 3);
    this.createButton("plus", 4);

    let infoService = new Service.AccessoryInformation();
    infoService.setCharacteristic(Characteristic.Manufacturer, "Senic");
    infoService.setCharacteristic(Characteristic.Model, "Nuimo Click");
    infoService.setCharacteristic(Characteristic.SerialNumber, "1337");
    this.services.push(infoService);

    this.doBrightnessUpLoop = false;
    this.doBrightnessDownLoop = false;
    this.brightnessInterval = null;
    this.lightsCacheValues = [];
    this.lastLightsCacheUpdateTime = 0;
  }

  setHasFoundAllLightIds(){
    this.hasFoundAllLightIds = this.controlLightNames.length <= 0 && this.controlLightIds.length <= 0;
  }

  createButton(subType, buttonIndex){
    let service = new Service.StatelessProgrammableSwitch("Nuimo Click", subType);
    this.services.push(service);
    this.buttonMap[buttonIndex] = service;
    //service.getCharacteristic(Characteristic.Name)
    //  .setValue(subType);
    service.getCharacteristic(Characteristic.ProgrammableSwitchEvent)
      .setProps({
        minValue: Characteristic.ProgrammableSwitchEvent.SINGLE_PRESS,
        maxValue: Characteristic.ProgrammableSwitchEvent.SINGLE_PRESS
      });
    service.getCharacteristic(Characteristic.ServiceLabelIndex)
      .setValue(buttonIndex);
  }

  onUpdateState(pressedButtons, deviceId){
    if(this.deviceId && deviceId != this.deviceId) return;
    if(pressedButtons.includes(1)){
      this.fireButtonEvent(1); // o
    }
    if(pressedButtons.includes(3)){
      this.fireButtonEvent(2); // *
    }
    if(pressedButtons.includes(5)){
      this.fireButtonEvent(3); // -
    }
    if(pressedButtons.includes(7)){
      this.fireButtonEvent(4); // +
    }

    if(pressedButtons.length == 1){
      if(pressedButtons[0] == 1){ // o
        this.toggleLight();
      }else if(pressedButtons[0] == 5){ // -
        this.deltaBrightness(-20);
        this.doBrightnessDownLoop = true;
        this.startBrightnessLoop();
      }else if(pressedButtons[0] == 7){ // +
        this.deltaBrightness(20);
        this.doBrightnessUpLoop = true;
        this.startBrightnessLoop();
      }
    }else{
      this.doBrightnessUpLoop = this.doBrightnessDownLoop = false;
      this.stopBrightnessLoop();
    }
  }

  fireButtonEvent(buttonIndex){
    let service = this.buttonMap[buttonIndex];
    service.updateCharacteristic(Characteristic.ProgrammableSwitchEvent, 0);
  }

  startBrightnessLoop(){
    if(this.brightnessInterval != null) return;
    this.brightnessInterval = setInterval(_ => {
      if(this.doBrightnessUpLoop){
        this.deltaBrightness(10);
      }else if(this.doBrightnessDownLoop){
        this.deltaBrightness(-10);
      }
    }, 300);
  }

  stopBrightnessLoop(){
    if(this.brightnessInterval == null) return;
    clearInterval(this.brightnessInterval);
    this.brightnessInterval = null;
  }

  accessoriesResponse(accessories){
    if(this.hasFoundAllLightIds) return;
    for(const accessory of accessories){
      let index = this.controlLightIds.indexOf(accessory.aid);
      if(index >= 0){
        this.addLightAccessory(accessory);
        this.controlLightIds.splice(index, 1);
        continue;
      }
      for(const service of accessory.services){
        if(isLightBulb(service.type)){
          for(const characteristic of service.characteristics){
            if(isNameCharacteristic(characteristic.type)){
              let index = this.controlLightNames.indexOf(characteristic.value);
              if(index >= 0){
                this.addLightAccessory(accessory);
                this.controlLightNames.splice(index, 1);
              }
            }
          }
        }
      }
    }

    this.setHasFoundAllLightIds();
  }

  addLightAccessory(accessory){
    if(this.controlLightCharacteristics.find(c => c.aid == accessory.aid)) return; //already exists
    let lightData = {
      aid: accessory.aid,
    };
    for(const service of accessory.services){
      if(isLightBulb(service.type)){
        for(const characteristic of service.characteristics){
          if(isOnCharacteristic(characteristic.type)){
            lightData.onIid = characteristic.iid;
          }else if(isBrightnessCharacteristic(characteristic.type)){
            lightData.brightnessIid = characteristic.iid;
          }else if(isHueCharacteristic(characteristic.type)){
            lightData.hueIid = characteristic.iid;
          }else if(isSaturationCharacteristic(characteristic.type)){
            lightData.saturationIid = characteristic.iid;
          }
        }
      }
    }
    this.controlLightCharacteristics.push(lightData);
  }

  async toggleLight(){
    if(this.controlLightCharacteristics.length <= 0) return;
    let allStates = [];
    let response = await hapCall("accessories");
    for(const accessory of response.accessories){
      for(const ids of this.controlLightCharacteristics){
        if(accessory.aid == ids.aid){
          let onCharacteristic = this.findIid(accessory, ids.onIid);
          allStates.push(onCharacteristic.value);
        }
      }
    }
    let onCount = allStates.filter(v => v).length;
    let offCount = allStates.filter(v => !v).length;
    let newState = onCount < offCount;

    await this.setLightOnState(newState);
  }

  async setLightOnState(newState){
    let putCharacteristics = [];
    for(const accessory of this.controlLightCharacteristics){
      putCharacteristics.push({
        aid: accessory.aid,
        iid: accessory.onIid,
        value: newState,
        status: 0
      });
    }
    let newLightsCache = [];
    for(let i=0; i<this.controlLightCharacteristics.length; i++){
      let oldBrightness = 100;
      let oldCache = this.lightsCacheValues[i];
      if(oldCache) oldBrightness = oldCache.brightness;
      newLightsCache.push({
        brightness: oldBrightness,
        on: newState,
      });
    }
    let putData = {
      characteristics: putCharacteristics,
    }
    this.updateLightsCache(newLightsCache);
    await hapCall("characteristics", putData);
  }

  async deltaBrightness(delta){
    if(this.controlLightCharacteristics.length <= 0) return;
    if(this.lightsCacheValues.length != this.controlLightCharacteristics.length || Date.now() - this.lastLightsCacheUpdateTime > 2000){
      let response = await hapCall("accessories");
      let newLightsCache = [];
      for(const ids of this.controlLightCharacteristics){
        let foundBrightness = 100;
        let foundOn = false;
        for(const accessory of response.accessories){
          if(ids.aid == accessory.aid){
            let brightnessCharacteristic = this.findIid(accessory, ids.brightnessIid);
            foundBrightness = brightnessCharacteristic.value;
            let onCharacteristic = this.findIid(accessory, ids.onIid);
            foundOn = onCharacteristic.value;
          }
        }
        newLightsCache.push({
          brightness: foundBrightness,
          on: foundOn,
        });
      }
      this.updateLightsCache(newLightsCache);
    }
    let putCharacteristics = [];
    let newLightsCache = [];
    for(let i=0; i<this.controlLightCharacteristics.length; i++){
      let ids = this.controlLightCharacteristics[i];
      let oldValues = this.lightsCacheValues[i];
      let oldBrightness = oldValues.brightness;
      let oldOn = oldValues.on;

      let newBrightness = oldOn ? oldBrightness + delta : 0;
      newBrightness = Math.min(100, Math.max(0, newBrightness));
      let newOn = oldOn || delta > 0;
      if(oldOn && delta < 0){
        newOn = newBrightness > 0 || oldBrightness > 0;
      }
      newLightsCache.push({
        brightness: newBrightness,
        on: newOn,
      });
      if(newBrightness != oldBrightness){ //only send request when there is an actual change
        putCharacteristics.push({
          aid: ids.aid,
          iid: ids.brightnessIid,
          value: newBrightness,
          status: 0
        });
      }
      if(newOn != oldOn){
        putCharacteristics.push({
          aid: ids.aid,
          iid: ids.onIid,
          value: newOn,
          status: 0
        });
      }
    }
    this.updateLightsCache(newLightsCache);

    if(putCharacteristics.length >= 0){
      let putData = {
        characteristics: putCharacteristics,
      }
      await hapCall("characteristics", putData);
    }
  }

  updateLightsCache(newValues){
    this.lightsCacheValues = newValues;
    this.lastLightsCacheUpdateTime = Date.now();
  }

  findIid(accessory, iid){
    for(const service of accessory.services){
      if(service.iid == iid) return service;
      for(const characteristic of service.characteristics){
        if(characteristic.iid == iid) return characteristic;
      }
    }
  }

  getServices(){
    return this.services;
  }
}
