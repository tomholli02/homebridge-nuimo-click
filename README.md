
This is a homebridge plugin for the [Nuimo Click](https://www.senic.com/en/nuimo-click).

# Usage
```json
"platforms": [
    {
        "platform": "NuimoClick",
        "logIds": true,
        "devices": [
            {
	            "name": "Click Living Room",
	            "id": 325251,
	            "controlLights": ["Living Room Light 1", "Living Room Light 2"]
            },{
	            "name": "Click Bedroom",
	            "id": 325892,
	            "controlLightIds": [23,24,25]
            }
        ]
    }
]
```

- **platform** (required) must be `NuimoClick`
- **logIds** (optional) set this to true to log device ids in order to figure out what device id your Click has.
- **devices** (optional), a list of device ids to listen for. When omitted, the plugin will create one HomeKit device that responds to all Nuimo Click commands. Regardless of the device id.

# Controlling lights

This plugin has support for automatically linking a Nuimo Click to certain lights that are controlled by homekit. For this to work you have to run homebridge using the insecure `-I` flag.
When adding a Click to this plugin, it gets exposed as a switch in your home app. However, the support for assigning buttons to accesories is not very elegant. You can only assign one button to an action. So it is not possible have a button toggle a light on or off based on its current state. It is also not possible to increment the brightness of a light relative to its current brightness. It can be done using external apps but the setup process is very tedious.

In the configuration of this plusing you can specify the name of your lights to a Nuimo Click using the `controlLights` field. You should specify the default device name as it appears in your home app. Not the name that you entered yourself.

Additionally, you can also specify the device ids as they appear on `http://<your homebridge ip>:<your homebridge port>/accessories`.

When set up properly, you can toggle the lights on or off using the O button on your Nuimo Click. Holding down the + and - buttons increases/decreases the brightness. You can also click + and - briefly to increase/decrease the brightness by 20%.

Native HomeKit devices that haven't been created using Homebridge are not supported unfortunately. Though you can controll these devices by assigning an action to the Nuimo Click in the home app.